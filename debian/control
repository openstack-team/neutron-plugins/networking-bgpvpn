Source: networking-bgpvpn
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
 python3-sphinxcontrib.blockdiag,
 python3-sphinxcontrib.seqdiag,
Build-Depends-Indep:
 openstack-dashboard,
 python3-bcrypt,
 python3-coverage,
 python3-debtcollector,
 python3-django-horizon,
 python3-isort,
 python3-networking-bagpipe,
 python3-neutron (>= 23.0.0~b2),
 python3-neutron-lib,
 python3-openstackdocstheme,
 python3-os-testr,
 python3-oslo.config,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.utils,
 python3-oslosphinx,
 python3-oslotest,
 python3-psycopg2,
 python3-pymysql,
 python3-stestr,
 python3-subunit,
 python3-tempest,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 python3-webob,
 python3-webtest,
 subunit,
Standards-Version: 4.5.0
Homepage: http://github.com/openstack/networking-bgpvpn
Vcs-Browser: https://salsa.debian.org/openstack-team/neutron-plugins/networking-bgpvpn
Vcs-Git: https://salsa.debian.org/openstack-team/neutron-plugins/networking-bgpvpn.git

Package: networking-bgpvpn-doc
Build-Profiles: <!nodoc>
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack virtual network service - BGP-MPLS VPN Extension - doc
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides an API and Framework to interconnect BGP/MPLS VPNs to
 Openstack Neutron networks, routers and ports.
 .
 The Border Gateway Protocol and Multi-Protocol Label Switching are widely used
 Wide Area Networking technologies. The primary purpose of this project is to
 allow attachment of Neutron networks and/or routers to VPNs built in carrier
 provided WANs using these standard protocols. An additional purpose of this
 project is to enable the use of these technologies within the Neutron
 networking environment.
 .
 A vendor-neutral API and data model are provided such that multiple SDN
 controllers may be used as backends, while offering the same tenant facing
 API. A reference implementation working along with Neutron reference drivers
 is also provided.
 .
 This package contains the documentation.

Package: python3-networking-bgpvpn
Architecture: all
Section: python
Depends:
 python3-bcrypt,
 python3-debtcollector,
 python3-django-horizon,
 python3-networking-bagpipe,
 python3-neutron (>= 23.0.0~b2),
 python3-neutron-lib,
 python3-oslo.config,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.utils,
 python3-pbr,
 python3-sphinx,
 python3-sphinxcontrib.blockdiag,
 python3-sphinxcontrib.seqdiag,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 networking-bgpvpn-doc,
Description: OpenStack virtual network service - BGP-MPLS VPN Extension - Python 3.x
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides an API and Framework to interconnect BGP/MPLS VPNs to
 Openstack Neutron networks, routers and ports.
 .
 The Border Gateway Protocol and Multi-Protocol Label Switching are widely used
 Wide Area Networking technologies. The primary purpose of this project is to
 allow attachment of Neutron networks and/or routers to VPNs built in carrier
 provided WANs using these standard protocols. An additional purpose of this
 project is to enable the use of these technologies within the Neutron
 networking environment.
 .
 A vendor-neutral API and data model are provided such that multiple SDN
 controllers may be used as backends, while offering the same tenant facing
 API. A reference implementation working along with Neutron reference drivers
 is also provided.
